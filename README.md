RADMC-3D tutorial
=================
Created Donnerstag 25 Januar 2018

2 hours length

System requirements
-------------------
* GCC with gfortran (xcode)
	On mac: when gfortran is installed from MacPorts (gcc6 or gcc7), use: gfortran-mp-6
		alias gfortran=/opt/local/bin/gfortran-mp-6
	On linux: dnf install / apt install gcc-gfortran
* python 2.7
* Ipython and Ipython notebook
* Jupyter notebook
        Anaconda package (https://www.anaconda.com/download/)
        Using linux package manager: link [https://www.digitalocean.com/community/tutorials/how-to-set-up-a-jupyter-notebo$]
* scipy
* astropy
* matplotlib
* ds9 (optional)


Content Plan
------------
* Introduction to the code; examples of what the code can do; input files, important parameters, compiling and installing the code
* Dust continuum:
	* Example of a parametric protoplanetary disk in 2D, and/or simulation data
	* Dust opacities
	* Images and SED
* Line emission:
	* Different methods for computing the level population (e.g. LTE, LVG, ...)
	* Example, e.g. keplerian line profiles of CO emission from a disk.
